import React, {Component} from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import LoginForm from './src/assigment2/login';
import SignUp from './src/assigment2/signup';
import WelcomeScreen from './src/assigment2/welcomescreen';
import ResetPassword from './src/assigment2/resetpassword';
// import Movie from './src/container/movie';
// import Delete from './src/container/delete';
// import State from './src/container/state';

const Stack = createStackNavigator();

const App: () => Node = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="welcomescreen"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="login" component={LoginForm} />
        <Stack.Screen name="signup" component={SignUp} />
        <Stack.Screen name="welcomescreen" component={WelcomeScreen} />
        <Stack.Screen name="resetpassword" component={ResetPassword} />
        {/* <Stack.Screen name="movie" component={Movie}/> */}
        {/* <Stack.Screen name="delete" component={Delete}/> */}
        {/* <Stack.Screen name="state" component={State}/> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

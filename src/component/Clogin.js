import React, {Component} from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';

class CButton extends Component {
  render() {
    return (
      <TouchableOpacity style={styles.botton} onPress={this.props.onPress}>
        <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20}}>
          {this.props.Text}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  botton: {
    marginTop: 10,
    backgroundColor: 'black',
    borderRadius: 5,
    width: 100,
    alignItems: 'center',
    elevation: 4,
    width: '90%',
    paddingVertical: 10,
  },
});

export default CButton;

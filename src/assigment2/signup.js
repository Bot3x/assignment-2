import  React,{Component} from 'react';
import {StyleSheet,Text,View,TextInput,TouchableOpacity,Image} from 'react-native';
import logo from '../asset/panda.png';
import background from '../asset/background1.jpg';
import Icon from 'react-native-vector-icons/AntDesign';

export default class App extends Component{
 constructor(){
    super();
    this.state = {showPassword:true};
 }
show=()=>{
this.setState({
    showPassword: !this.state.showPassword
 })
}
buttonPress= ()=>{
  this.setState({
    username:'',
    email:'',
    password:'',
    phone:''

  })
}
  render() {
    const {showPassword} = this.state
    return (
      <View style={styles.assigment2}>
          <Image style={styles.background} source={background}/>
        <Image style={styles.logo} source={logo}/>
        <Text style={{fontSize:25,fontWeight:'bold'}}>SIGN UP PANDA COMMUNITY</Text> 
        <View style={styles.input}>
            <Icon name='user' size={30} color='black'/>
            <TextInput value={this.state.username} placeholder='Username'onChangeText ={(typing)=>this.setState({username:typing})}/>
        </View>
        <View style={styles.input}>
            <Icon name='mail' size={30} color='black'/>
            <TextInput value={this.state.email} placeholder='Email'onChangeText ={(typing)=>this.setState({email:typing})}/>
        </View>
        <View style={styles.input}>
            <Icon name='key' size={30} color='black'/>
            <TextInput value={this.state.password} secureTextEntry={showPassword} placeholder='Password'onChangeText ={(typing)=>this.setState({password:typing})} style={{flex: 1}}/>
            <TouchableOpacity onPress={this.show}>
              <Icon name ='eye' size={30} color='black' style='' />
            </TouchableOpacity>
        </View>
        <View style={styles.input}>
            <Icon name='phone' size={30} color='black'/>
            <TextInput value={this.state.phone} placeholder="Phone Number"onChangeText ={(typing)=>this.setState({phone:typing})}/>
        </View>
        <TouchableOpacity onPress={this.buttonPress} style={{borderWidth:2,borderColor:'black',width:300,alignItems:'center',backgroundColor:'green',marginTop:10,borderRadius:10,}}>
            <Text style={{color:'white',fontWeight:'bold',fontSize:20,}}>SignUp</Text>
          </TouchableOpacity>
          <View style={{flexDirection:'row',alignItems:'center'}}>
          <Text style={{fontSize:20,color:'grey',fontWeight:'bold',}}>Forget Password?</Text>
            <TouchableOpacity>
            <Text style={{color:'blue',fontSize:20,fontWeight:'bold'}} title="Go Reset Password"
                    onPress={() => this.props.navigation.navigate('resetpassword')}>Reset Password</Text>
            </TouchableOpacity>
            </View>
            <View style={{flexDirection:'row',alignItems:'center'}}>
            <Text style={{fontSize:20,color:'grey',fontWeight:'bold',}}>Already SignUp?</Text>
            <TouchableOpacity>
                <Text style={{color:'blue',fontSize:20,fontWeight:'bold'}} title="Go Login"
                    onPress={() => this.props.navigation.navigate('login')}>Login</Text>
            </TouchableOpacity>

          </View>
          
        
        </View>
    )
  }
}
const styles = StyleSheet.create({
  assigment2: {
    flex:1,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: "#F0FFFF"
  },
  input:{
    alignItems:'center',
    flexDirection:'row',
    paddingLeft: 25, 
    width:300,
    height: 50, 
    margin: 10, 
    borderWidth: 1,
    borderRadius:5,
    borderColor:'black',
    backgroundColor:'white',
    elevation: 15,
    paddingLeft:15,


  },
  logo:{
    width:300,
    height:200,
    

},
  background:{
  flex: 1,
  resizeMode: 'cover',
  justifyContent: 'center',
  alignItems: 'center',
  position: 'absolute',
  width: '100%',
  height: '100%',

},


})
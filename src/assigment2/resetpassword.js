import  React,{Component} from 'react';
import {StyleSheet,Text,View,TextInput,TouchableOpacity,Image} from 'react-native';
import logo from '../asset/panda.png';
import background from '../asset/background1.jpg';
import Icon from 'react-native-vector-icons/AntDesign';

export default class App extends Component{
constructor(){
  super();
  this.state = {showPassword:true}
}
show=()=>{
  this.setState({
    showPassword: !this.state.showPassword
  })
}

buttonPress= ()=>{
  this.setState({
    reset:'',
    resetpassword:'',
  })
}

  render() {
    const {showPassword} = this.state
    return (
      <View style={styles.assigment2}>
        <Image style={styles.background} source={background}/>
        <Image style={styles.logo} source={logo}/>
        <Text style={{fontSize:25,fontWeight:'bold'}}>RESET PASSWORD</Text> 
        <View style={styles.input}>
            <Icon name='key' size={30} color='black'/>
            <TextInput value={this.state.reset} secureTextEntry={showPassword} placeholder='Password'onChangeText ={(typing)=>this.setState({reset:typing})}/>
        </View>
        <View style={styles.input}>
            <Icon name='key' size={30} color='black'/>
            <TextInput value={this.state.resetpassword} secureTextEntry={showPassword} placeholder='Confirm Password'onChangeText ={(typing)=>this.setState({resetpassword:typing})}/>
        </View>
          <TouchableOpacity style={styles.botton} onPress={this.buttonPress}>
            <Text style={{color:'white',fontWeight:'bold',fontSize:20}}>Reset Password</Text>
          </TouchableOpacity>
              <TouchableOpacity>
                <Text style={{color:'blue',fontSize:20,fontWeight:'bold'}} title="Go SignUp"
                    onPress={() => this.props.navigation.navigate('login')}>Back to Login</Text>
            </TouchableOpacity>
         </View>
    )
  }
}
const styles = StyleSheet.create({
  assigment2: {
    flex:1,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: "#F0FFFF"
  },
  input:{
    alignItems:'center',
    flexDirection:'row',
    paddingLeft: 25, 
    width:300,
    height: 50, 
    margin: 10, 
    borderWidth: 1,
    borderRadius:5,
    borderColor:'black',
    backgroundColor:'white',
    elevation: 15,
    paddingLeft:15,


  },
  logo:{
   
    marginBottom:20,
    alignSelf:'center',
    width:300,
    height:150,
},
  background:{
  flex: 1,
  resizeMode: 'cover',
  justifyContent: 'center',
  alignItems: 'center',
  position: 'absolute',
  width: '100%',
  height: '100%',

},
  botton:{
  marginTop: 30,
  borderWidth: 2,
  backgroundColor:'green',
  borderRadius:5,
  width:100,
  alignItems: 'center',
  elevation:10,
  width:300,
  
}
  })
import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import logo from '../asset/panda.png';
import background from '../asset/background1.jpg';
import Icon from 'react-native-vector-icons/AntDesign';
import Clogin from '../component/Clogin';

export default class App extends Component {
  constructor() {
    super();
    this.state = {showPassword: true};
  }
  show = () => {
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };

  buttonPress = () => {
    this.setState({
      email: '',
      password: '',
    });
  };

  render() {
    const {showPassword} = this.state;
    return (
      <View style={styles.assigment2}>
        <Image style={styles.background} source={background} />
        <Image style={styles.logo} source={logo} />
        <Text style={{fontSize: 25, fontWeight: 'bold'}}>
          LOGIN PANDA COMUNNITY
        </Text>
        <View style={styles.input}>
          <Icon name="user" size={30} color="black" />
          <TextInput
            value={this.state.email}
            placeholder="Email or Username"
            onChangeText={typing => this.setState({email: typing})}
          />
        </View>
        <View style={styles.input}>
          <Icon name="key" size={30} color="black" />
          <TextInput
            value={this.state.password}
            secureTextEntry={showPassword}
            placeholder="Password"
            onChangeText={typing => this.setState({password: typing})}
            style={{flex: 1}}
          />
          <TouchableOpacity onPress={this.show}>
            <Icon name="eye" size={30} color="black" />
          </TouchableOpacity>
        </View>
        {/* <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20}}>
            Login
          </Text> */}
        <Clogin Text="Login Bro" />
        <Clogin
          Text="SignUp"
          onPress={() => this.props.navigation.navigate('signup')}
        />

        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={{color: 'grey', fontWeight: 'bold', fontSize: 20}}>
            Forget Password
          </Text>
          <TouchableOpacity>
            <Text
              style={{color: 'blue', fontSize: 20, fontWeight: 'bold'}}
              title="Go Reset Password"
              onPress={() => this.props.navigation.navigate('resetpassword')}>
              Reset Password
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={{color: 'grey', fontWeight: 'bold', fontSize: 20}}>
            Dont have a Account
          </Text>
          <TouchableOpacity>
            <Text
              style={{color: 'blue', fontSize: 20, fontWeight: 'bold'}}
              title="Go SignUp"
              onPress={() => this.props.navigation.navigate('signup')}>
              Sign Up
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  assigment2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F0FFFF',
  },
  input: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 25,
    width: 300,
    height: 50,
    margin: 10,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: 'black',
    backgroundColor: 'white',
    elevation: 15,
    paddingLeft: 15,
  },
  logo: {
    marginBottom: 20,
    alignSelf: 'center',
    width: 300,
    height: 150,
  },
  background: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  botton: {
    marginTop: 30,
    borderWidth: 2,
    backgroundColor: 'green',
    borderRadius: 5,
    width: 100,
    alignItems: 'center',
    elevation: 10,
    width: 300,
  },
});

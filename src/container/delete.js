import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  TextInput,
  TouchableOpacity,
  DeviceEventEmitter,
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      data: [
        {id: 1, nama: 'Arif', alamat: 'Jakarta'},
        {id: 2, nama: 'Yono', alamat: 'Jawa'},
        {id: 3, nama: 'Yini', alamat: 'Brebes'},
      ],
      newId: '',
      newNama: '',
      newAlamat: '',
    };
  }

  tambah = () => {
    const tampungan = this.state.data;
    tampungan.push({
      id: this.state.newId,
      nama: this.state.newNama,
      alamat: this.state.newAlamat,
    });
    this.setState({
      data: tampungan,
      newId: '',
      newNama: '',
      newAlamat: '',
    });
  };
  hapus = penghapusan => {
    const aksiHapus = this.state.data.filter(data => data.id != penghapusan.id);
    this.setState({
      data: aksiHapus,
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            backgroundColor: 'black',
            borderRadius: 10,
            marginTop: 20,
            alignItems: 'center',
          }}>
          <TextInput
            style={styles.input}
            placeholder="Id"
            onChangeText={input => this.setState({newId: input})}
            value={this.state.newId}
          />
          <TextInput
            style={styles.input}
            placeholder="Nama"
            onChangeText={input => this.setState({newNama: input})}
            value={this.state.newNama}
          />
          <TextInput
            style={styles.input}
            placeholder="Alamat"
            onChangeText={input => this.setState({newAlamat: input})}
            value={this.state.newAlamat}
          />
          <Button title="Tambah" onPress={this.tambah} />
        </View>
        <View style={styles.table}>
          <View style={styles.inTable}>
            <Text style={styles.number}>#</Text>
            <Text style={styles.nama}>Nama</Text>
            <Text style={styles.alamat}>Alamat</Text>
            <Text style={styles.delete}>Action</Text>
          </View>
          {this.state.data.map((value, index) => {
            return (
              <View key={index} style={styles.onTable}>
                <Text style={[styles.number, styles.data]}>{index + 1}</Text>
                <Text style={[styles.nama, styles.data]}>{value.nama}</Text>
                <Text style={[styles.alamat, styles.data]}>{value.alamat}</Text>
                <TouchableOpacity onPress={() => this.hapus(value)}>
                  <Icon name="trash" size={30} color="black" />
                </TouchableOpacity>
              </View>
            );
          })}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    flex: 1,
    justifyContent: 'center',
  },
  table: {
    backgroundColor: 'blue',
    width: '90%',
    marginHorizontal: 20,
  },
  inTable: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: 'red',
    borderWidth: 2,
    height: 60,
    alignItems: 'center',
    borderRadius: 5,
  },
  onTable: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 60,
    borderWidth: 1,
    alignItems: 'center',
    borderRadius: 10,
  },
  name: {
    fontSize: 20,
  },
  address: {
    fontSize: 20,
  },
  number: {
    fontSize: 20,
  },
  input: {
    borderWidth: 2,
    width: '50%',
    marginHorizontal: 20,
    marginTop: 20,
    borderRadius: 5,
    backgroundColor: 'white',
  },
});
